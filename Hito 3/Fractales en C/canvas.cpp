#include "canvas.h"
#include <complex> //Están definidas las funciones para las operaciones con números complejos
#include <QLabel> //Utilizamos QLabel para mostrar imagenes animadas en formato GIF
#include <QSlider> //Para tener un slider horizontal o vertical (en este caso horizontal)
#include <QToolBar> //Proporciona un panel móvil que contiene un conjunto de controles

CCanvas::CCanvas(QWidget *parent) : QMainWindow(parent) //En nuestro metodo constructor
{
    QToolBar* pToolBar = new QToolBar(this); //Creamos una barra de herramientas (ToolBar)
    pToolBar->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea); //La seteamos
    addToolBar(pToolBar);

    QLabel* plblBrightness = new QLabel("Brightness: ", this); //Creamos un label para controlar el brillo
    pToolBar->addWidget(plblBrightness); //Lo añademos a nuestra ToolBar

    QSlider* pwBrightness = new QSlider(Qt::Horizontal, this); //Seteamos un slider horizontal para el brillo
    pToolBar->addWidget(pwBrightness); //Añadimos el slider a nuestra ToolBar
    pToolBar->addSeparator();

    pwBrightness->setRange(0, 10); //Creamos el rango del brillo

    connect(pwBrightness, &QSlider::sliderMoved, //Conectamos el evento de mover el slider con el valor que el brillo toma
            [=](int nVal) { m_nBrightness = nVal; update(); } );

    QLabel* plblSensitivity = new QLabel("Sensitivity: ", this); //Hacemos exactamente lo mismo con la sensibilidad
    pToolBar->addWidget(plblSensitivity);

    QSlider* pwSensitivity = new QSlider(Qt::Horizontal, this);
    pToolBar->addWidget(plblSensitivity);

    pwSensitivity->setRange(20, 50);
    pwSensitivity->setValue(m_nSensitivity);

    connect(pwSensitivity, &QSlider::sliderMoved,
            [=](int nVal) { m_nSensitivity = nVal; update(); } );


}


int CCanvas::getColorValue(double ptX, double ptY) //Este metodo determina la posición, el brillo y la sensibilidad a medida que se van recorriendo los for de paintEvent
{
 std::complex<double> complexPoint(ptX / width() - 1.5, //Esto respecta con la sucesión
                                  ptY / height() - 0.5); //(Termino de sucesion recursiva)

 std::complex<double> complexZ(0, 0); //(Termino incial)

 int nBrightness = m_nBrightness;

 while((abs(complexZ) < 2 ) && (nBrightness <= m_nSensitivity )){ //Se van ajustando los parametros y ocurre la magia
     complexZ = complexZ * complexZ + complexPoint;
     nBrightness++; //Se va correlacionando el brillo con la recurción
 }

 if(nBrightness < m_nSensitivity)
     return static_cast<int>(( 255 * nBrightness) / m_nSensitivity);
 else
     return 0;
}


void CCanvas::paintEvent(QPaintEvent*) //Con este metodo dibujamos los puntos
{
    QPainter oPainter(this);
    oPainter.setRenderHint(QPainter::Antialiasing); //Creamos y seteamos qpainter

    QPen oPen; //inicializamos un pen
    int nColorValue = 0; //comenzamos con 0

    for(int ptX = 0; ptX < width(); ++ptX){ //Recorremos en eje x
        for(int ptY = 0; ptY < height(); ++ptY){ //Recorremos en eje y
            nColorValue = getColorValue(static_cast<double>(ptY), //Obtenemos los colores respectivos
                                        static_cast<double>(ptX));

            oPen.setColor(QColor(nColorValue, 0, 0)); //Seteamos el color en el lapiz
            oPainter.setPen(oPen);
            oPainter.drawPoint(ptX, ptY); //Dibujamos el punto
        }
    }
}
