#include "fractal.h"
#include "ui_fractal.h"

//Implementacion clase Fractal

Fractal::Fractal(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Fractal)
{
    ui->setupUi(this);
    setWindowTitle("Fractales en C++"); //Nombre de la ventana de aplicación
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
}

Fractal::~Fractal()
{
    delete ui;
}


//Implementación Fractal Sierpinski

//Esta funcion recibe las coordenadas de los vertices y el nivel
void Fractal::trianguloSierpinski(int x1, int y1, int x2, int y2, int x3, int y3, int nivel){
    if(nivel == 1){//Si es nivel uno, entonces dibuja el triangulo principal
            dibujaTriangulo(x1,y1,x2,y2,x3,y3);
        } else{//Si no es nivel uno, entonces comienza a fractalizarse
            nivel--;
            //Calculamos los vertices de los nuevos triangulos
            trianguloSierpinski(x1 , y1 , (x1+x2)/2.0 , (y1+y2)/2.0 , (x1+x3)/2.0 , (y1+y3)/2.0 , nivel);
            trianguloSierpinski((x1+x2)/2.0 , (y1+y2)/2.0 , x2 , y2 , (x2+x3)/2.0 , (y2+y3)/2.0 , nivel);
            trianguloSierpinski((x1+x3)/2.0 , (y1+y3)/2.0 , (x2+x3)/2.0 , (y2+y3)/2.0 , x3 , y3 , nivel);
    }
}
//Esta funcion dibuja tres lineas entre cada uno de los vertices
void Fractal::dibujaTriangulo(int x1, int y1, int x2, int y2, int x3, int y3){
    QPen blackPen(Qt::magenta);
    blackPen.setWidth(2);

    line = scene->addLine(x1,y1,x2,y2);
    line = scene->addLine(x2,y2,x3,y3);
    line = scene->addLine(x1,y1,x3,y3);
}

//Implementación Fractal Koch

void Fractal::koch(struct punto p0, struct punto p1,int nivel){ //p0 es el primer punto y p1 es el ultimo punto
    QPen blackPen(Qt::yellow);
    blackPen.setWidth(6);

    double dx = p1.x - p0.x; // dx = distancia entre p0 y p1 en eje x
    double dy = p1.y - p0.y; // dy = distancia entre p0 y p1 en eje y
    double dist = sqrt(dx * dx + dy * dy); // dist = distancia entre p0 y p1
    double unit = dist/3;
    double angle = atan2(dy,dx); // calcula el angulo entre dx y dy en radianes

    struct punto pA;
    pA.x = p0.x + dx / 3;
    pA.y = p0.y + dy / 3;

    struct punto pB;
    pB.x = pA.x + cos(angle - M_PI/3) * unit; // angle - pi/3 nos ayuda a ubicar el punto pB, tanto en eje x
    pB.y = pA.y + sin(angle - M_PI/3) * unit; //como en eje y

    struct punto pC;
    pC.x = p1.x - dx / 3;
    pC.y = p1.y - dy / 3;

    if(nivel > 0){
        koch(p0,pA, nivel - 1);
        koch(pA,pB, nivel - 1);
        koch(pB,pC, nivel - 1);
        koch(pC,p1, nivel - 1);
    }else{
        line = scene->addLine(p0.x,p0.y,pA.x,pA.y);
        line = scene->addLine(pA.x,pA.y,pB.x,pB.y);
        line = scene->addLine(pB.x,pB.y,pC.x,pC.y);
        line = scene->addLine(pC.x,pC.y,p1.x,p1.y);
    }
}
//Implementación Fractal Tree

void Fractal::tree(struct punto p0, struct punto p1, int nivel){
    QPen blackPen(Qt::magenta);
    blackPen.setWidth(4);

    double dx,dy,dist,angle;
    dx = p1.x - p0.x; //es la distancia entre p0 y p1 en el eje x
    dy = p1.y - p0.y; //es la distancia entre p0 y p1 en el eje y
    dist = sqrt(dx*dx + dy*dy); //es la distancia total entre p0 y p1
    angle = atan2(dy,dx); //es el angulo entre dx y dy en radianes

    double ramaAngulo = M_PI/4; //ramaAngulo es el angulo en el que cada rama se divide
    double ramaPart = 0.5; //ramaPart es la razón de la longitud de cada rama respecto a la iteracion anterior

    double ramaLargo = dist * (1 - ramaPart); //es el largo de la nueva rama

    struct punto pA; //pA es el punto entre p0 y p1
    pA.x = p0.x + dx * ramaPart;
    pA.y = p0.y + dy * ramaPart;

    struct punto pB; //pB es el punto de la nueva rama derecha
    pB.x = pA.x + cos(angle + ramaAngulo) * ramaLargo; // coordenada x del punto b
    pB.y = pA.y + sin(angle + ramaAngulo) * ramaLargo; // coordenada y del punto b

    struct punto pC; //pC es el punto de la nueva rama izquierda
    pC.x = pA.x + cos(angle - ramaAngulo) * ramaLargo; // coordenada x del punto c
    pC.y = pA.y + sin(angle - ramaAngulo) * ramaLargo; // coordenada y del punto c

    //dibuja el "tronco" en cada iteración
    line = scene->addLine(p0.x,p0.y,pA.x,pA.y);

    if (nivel > 0){
        tree(pA,pB, nivel - 1);
        tree(pA,pC, nivel - 1);
    }else{
        line = scene->addLine(pB.x,pB.y,pA.x,pA.y);
        line = scene->addLine(pA.x,pA.y,pC.x,pC.y);
    }
}


