#ifndef CANVAS_H
#define CANVAS_H

#include <QMainWindow>
#include <QPainter>

QT_BEGIN_NAMESPACE
namespace Ui { class CCanvas; }
QT_END_NAMESPACE

class CCanvas : public QMainWindow
{
    Q_OBJECT

public:
    CCanvas(QWidget *parent = nullptr); //Se construye la ventana
    //Métodos
    int getColorValue(double ptX, double ptY); //Este metodo determina la posición, el brillo y la sensibilidad a medida que se van recorriendo ptX y ptY

protected:
    //Metodos
    void paintEvent(QPaintEvent* pEvent) override; //Con este metodo dibujamos los puntos

private:
    Ui::CCanvas *ui;
    //Atributos
    int m_nBrightness = 0;
    int m_nSensitivity = 34;

};
#endif // CANVAS_H
