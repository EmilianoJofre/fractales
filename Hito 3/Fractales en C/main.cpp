#include "fractal.h"
#include "canvas.h"

#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Fractal *fractal1;
    Fractal *fractal2;
    Fractal *fractal3;
    CCanvas fractal4;

    fractal1 = new Fractal();
    fractal2 = new Fractal();
    fractal3 = new Fractal();


    fractal1->trianguloSierpinski(450,100,100,900,900,900,8);

    struct punto p0,p1;
    p0.x = 100;
    p0.y = 700;
    p1.x = 900;
    p1.y = 700;

    fractal2->koch(p0,p1,4);
    struct punto q0,q1;
    q0.x = 500; // p0 es el punto que esta abajo a la mitad
    q0.y = 900;
    q1.x = 500; //p1 es el punto que esta arriba a la mitad
    q1.y = 0;
    fractal3->tree(q0,q1,9);

    fractal4.resize(800, 800);

    fractal1->show();
    fractal2->show();
    fractal3->show();
    fractal4.show();

    return a.exec();
}
