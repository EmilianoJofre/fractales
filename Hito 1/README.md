Nombre Proyecto: FRACTALES EN C

Descripci�n del proyecto: 

El objetivo principal del proyecto es crear diferentes fractales utilizando el lenguaje de programaci�n C, para esto primero modelamos la situaci�n matm�tica 
del fractal, analizamos su comportamiento de auto-similitud, y a partir de eso creamos las funciones para implementar una estructura fractal como imagen svg a partir de codigo C.
Entonces lo que hacen los programas es, b�sicamente crear recursivamente lineas de c�digo svg que todas juntas forman la imagen del Fractal.

Requisitos de Compilaci�n o Plataforma:

El programa puede compilarse con gcc en cualquier plataforma (windows, linox, mac).
El programa crea archivos.svg, por lo que es necesario contar con un svg viewer de manera que se puedan apreciar los fractales creados (en caso de no contar con uno puede utilizar un 
svg viewer online).

�C�mo instalar o compilar?:

1.) Bueno, primero que todo deber�a de descargar el repositorio que contiene (koch.c, sierpinski.c y fractalTree.c). Para eso puede dirigirse a GitLab donde se encuentra el repositorio
2.) Luego, para compilar estos archivos utilice los comandos:

comandos: gcc sierpinski.c -o sierpinski; gcc koch.c -o koch; gcc fractalTree.c -o fractalTree

De esta forma, se compilaran los tres archivos en una sola linea, se crearan ejecutables.exe con el mismo nombre de los archivos.c.
3.) Luego, se deben crear los archivos svg a partir de la ejecuci�n de los programas.c, como bien se indic� antes, cada programa crea codigo svg, por lo que ejecutaremos el siguiente 
comando para crear tres archivos svg que contengan la informaci�n de las lineas para dibujar un fractal.

comando:	./fractalTree.exe > fractalTree.svg; ./koch.exe > koch.svg; ./sierpinski.exe > sierpinski.svg

Con este comando se deber�an haber creado 3 archivos.svg con el respectivo nombre y contenido de cada fractal, ahora solo debe de abrir los archivos con un svg viewer, ya sea uno 
instalado en su pc, o uno online.

Datos integrantes (Grupo 6):

	Emiliano Jofre Vasquez
	Crist�bal Felipe Gonz�lez Jofr�
	Gabriel Antu Jofr� Sol�s
