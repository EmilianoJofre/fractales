#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define M_PI 3.14159265358979323846

struct punto{
	double x,y;
};

void tree(struct punto p0, struct punto p1, int nivel);

int main(){
	
	struct punto p0,p1;
	p0.x = 500; // p0 es el punto que esta abajo a la mitad
	p0.y = 1000;
	p1.x = 500; //p1 es el punto que esta arriba a la mitad
	p1.y = 0;
	

	
	printf("<svg width=\"1000\" height=\"1000\">\n");
	tree(p0,p1,8);
	printf("</svg>\n");
	
	return 0;
}

void tree(struct punto p0, struct punto p1, int nivel){
	double dx,dy,dist,angle;
	dx = p1.x - p0.x; //es la distancia entre p0 y p1 en el eje x
	dy = p1.y - p0.y; //es la distancia entre p0 y p1 en el eje y
	dist = sqrt(dx*dx + dy*dy); //es la distancia total entre p0 y p1
	angle = atan2(dy,dx); //es el angulo entre dx y dy en radianes
	
	double ramaAngulo = M_PI/4; //ramaAngulo es el angulo en el que cada rama se divide
	double ramaPart = 0.5; //ramaPart es la raz�n de la longitud de cada rama respecto a la iteracion anterior
	
	double ramaLargo = dist * (1 - ramaPart); //es el largo de la nueva rama
	
	struct punto pA; //pA es el punto entre p0 y p1
	pA.x = p0.x + dx * ramaPart; 
	pA.y = p0.y + dy * ramaPart;
	
	struct punto pB; //pB es el punto de la nueva rama derecha
	pB.x = pA.x + cos(angle + ramaAngulo) * ramaLargo; // coordenada x del punto b 
	pB.y = pA.y + sin(angle + ramaAngulo) * ramaLargo; // coordenada y del punto b
	
	struct punto pC; //pC es el punto de la nueva rama izquierda
	pC.x = pA.x + cos(angle - ramaAngulo) * ramaLargo; // coordenada x del punto c
	pC.y = pA.y + sin(angle - ramaAngulo) * ramaLargo; // coordenada y del punto c
	
	//dibuja el "tronco" en cada iteraci�n 
	printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"black\" stroke-width=\"1\" />\n",p0.x,p0.y,pA.x,pA.y);	
	
	if (nivel > 0){
		tree(pA,pB, nivel - 1);
		tree(pA,pC, nivel - 1);
	}else{
		printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"black\" stroke-width=\"1\" />\n",pB.x,pB.y,pA.x,pA.y);
		printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"black\" stroke-width=\"1\" />\n",pA.x,pA.y,pC.x,pC.y);
	}
}
