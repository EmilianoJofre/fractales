#include<stdio.h>

//Prototipo Funciones
void dibujaTriangulo(int x1, int y1, int x2, int y2, int x3, int y3);
void trianguloS(int x1, int y1, int x2, int y2, int x3, int y3, int nivel);

//Funcion main
int main(){

	int x1,y1,x2,y2,x3,y3,nivel;
	x1 = 450; // x1 y y1 son el vertice superior
	y1 = 100;
	x2 = 100; // x2 y y2 son el vertice izquierdo
	y2 = 900;
	x3 = 900; // x3 y y3 son el vertice derecho
	y3 = 900;
	nivel = 7;

	printf("<svg width=\"1000\" height=\"1000\">\n");
	trianguloS(x1,y1,x2,y2,x3,y3,nivel);
	printf("</svg>\n");

	return 0;
}


//Funciones

//Esta funcion recibe las coordenadas de los vertices y el nivel
void trianguloS(int x1, int y1, int x2, int y2, int x3, int y3, int nivel){
	if(nivel == 1){//Si es nivel uno, entonces dibuja el triangulo principal
		dibujaTriangulo(x1,y1,x2,y2,x3,y3);
	} else{//Si no es nivel uno, entonces comienza a fractalizarse
		nivel--;
		//Calculamos los vertices de los nuevos triangulos
		trianguloS(x1 , y1 , (x1+x2)/2.0 , (y1+y2)/2.0 , (x1+x3)/2.0 , (y1+y3)/2.0 , nivel);
		trianguloS((x1+x2)/2.0 , (y1+y2)/2.0 , x2 , y2 , (x2+x3)/2.0 , (y2+y3)/2.0 , nivel);
		trianguloS((x1+x3)/2.0 , (y1+y3)/2.0 , (x2+x3)/2.0 , (y2+y3)/2.0 , x3 , y3 , nivel);
	}
}
//Esta funcion dibuja tres lineas entre cada uno de los vertices
void dibujaTriangulo(int x1, int y1, int x2, int y2, int x3, int y3){
	printf("<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"red\" stroke-width=\"1\" />\n",x1,y1,x2,y2);
	printf("<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"blue\" stroke-width=\"1\" />\n",x2,y2,x3,y3);
	printf("<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"orange\" stroke-width=\"1\" />\n",x1,y1,x3,y3);
}
