#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define M_PI 3.14159265358979323846

struct punto{
	double x,y;
};

void koch(struct punto p0, struct punto p1,int nivel); 

int main(){
	struct punto p0,p1;
	p0.x = 100;
	p0.y = 700;
	p1.x = 900;
	p1.y = 700;
	
	printf("<svg width=\"1000\" height=\"1000\">\n");
	koch(p0,p1,3);
	printf("</svg>\n");
	
	
	return 0;
}

void koch(struct punto p0, struct punto p1,int nivel){ //p0 es el primer punto y p1 es el ultimo punto
	double dx = p1.x - p0.x; // dx = distancia entre p0 y p1 en eje x
	double dy = p1.y - p0.y; // dy = distancia entre p0 y p1 en eje y
	double dist = sqrt(dx * dx + dy * dy); // dist = distancia entre p0 y p1 
	double unit = dist/3; 
	double angle = atan2(dy,dx); // calcula el angulo entre dx y dy en radianes
	
	struct punto pA;
	pA.x = p0.x + dx / 3;
	pA.y = p0.y + dy / 3;
	
	struct punto pB;
	pB.x = pA.x + cos(angle - M_PI/3) * unit; // angle - pi/3 nos ayuda a ubicar el punto pB, tanto en eje x
	pB.y = pA.y + sin(angle - M_PI/3) * unit; //como en eje y
	
	struct punto pC;
	pC.x = p1.x - dx / 3;
	pC.y = p1.y - dy / 3;

	if(nivel > 0){
		koch(p0,pA, nivel - 1);
		koch(pA,pB, nivel - 1);
		koch(pB,pC, nivel - 1);
		koch(pC,p1, nivel - 1);
	}else{
		printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"red\" stroke-width=\"1\" />\n",p0.x,p0.y,pA.x,pA.y);
		printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"blue\" stroke-width=\"1\" />\n",pA.x,pA.y,pB.x,pB.y);
		printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"orange\" stroke-width=\"1\" />\n",pB.x,pB.y,pC.x,pC.y);
		printf("<line x1=\"%.0lf\" y1=\"%.0lf\" x2=\"%.0lf\" y2=\"%.0lf\" stroke=\"green\" stroke-width=\"1\" />\n",pC.x,pC.y,p1.x,p1.y);		
	}
	
	

	
}

