#ifndef FRACTAL_H
#define FRACTAL_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QString>
#include <QGraphicsScene>
#include <QPainter>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define M_PI 3.14159265358979323846

QT_BEGIN_NAMESPACE
namespace Ui { class Fractal; }
QT_END_NAMESPACE

struct punto{
    int x,y;
};

class Fractal : public QDialog
{
    Q_OBJECT

public:
    Fractal(QWidget *parent = nullptr);
    ~Fractal();

    //Métodos Fractal Sierpinski
    void trianguloSierpinski(int x1, int y1, int x2, int y2, int x3, int y3, int nivel);
    void dibujaTriangulo(int x1, int y1, int x2, int y2, int x3, int y3);
    //Métodos Fractal Tree
    void tree(struct punto p0, struct punto p1, int nivel);
    //Métodos Fractal Koch
    void koch(struct punto p0, struct punto p1,int nivel);

private:
    int m_nBrightness = 0;
    int m_nSensitivity = 34;
    punto p1,p2,p3;
    int nivel;
    Ui::Fractal *ui;
    QGraphicsScene *scene;
    QGraphicsLineItem *line;
};
#endif // FRACTAL_H
