Nombre Proyecto: FRACTALES EN C

Descripci�n del proyecto: 

Este proyecto consiste en un programa que grafica fractales, esto lo hace por medio de las ventajas graficas que nos da Qt, se dibujan lineas de manera recursiva, las cuales generan una figura geometrica llamada fractal.

Requisitos de Compilaci�n o Plataforma:

El programa puede compilarse y editarse con Qt Creator, utilizando c++ y Qt

�C�mo instalar o compilar?:

Para ejectuar el programa basta con abrir el proyecto en Qt Creator y correrlo. El programa generará tres ventanas con los 3 fractales creados anteriormente.

Datos integrantes (Grupo 6):

	Emiliano Jofre Vasquez
	Crist�bal Felipe Gonz�lez Jofr�
	Gabriel Antu Jofr� Sol�s
